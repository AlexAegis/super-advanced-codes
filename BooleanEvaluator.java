import com.*;
import org.*;
import java.*;
import javax.*;

public class BooleanUtil {

	public static final boolean TRUE = new BooleanFactory().getTrue().booleanValue();
	public static final boolean FALSE = new BooleanFactory().getFalse().booleanValue();

	public static boolean isTrue(Boolean input) {
		if (input == TRUE){
			return TRUE;
		} else {
			return FALSE;
		}
	}
}

public class BooleanFactory {

	public static Boolean getTrue() {
		return new Boolean(true);
	}
	public static Boolean getFalse() {
		return new Boolean(false);
	}
}